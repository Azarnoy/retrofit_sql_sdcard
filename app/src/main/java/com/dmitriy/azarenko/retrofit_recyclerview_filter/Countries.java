package com.dmitriy.azarenko.retrofit_recyclerview_filter;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;


public class Countries implements Serializable {

    String name;
    String capital;
    String region;
    String subregion;
    int population;
    int image;
    public List<String> languages;
    String[] timezones;

    public Countries(String name, String capital, String region, String subregion, int population,int image, List<String> languages, String[] timezones) {
        this.name = name;
        this.capital = capital;
        this.region = region;
        this.subregion = subregion;
        this.population = population;
        this.image = image;
        this.languages = languages;
        this.timezones = timezones;
    }




    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSubregion() {
        return subregion;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public String getLanguagesList() {
        String languagesList = "";
        for (String language : languages) {
            Locale locale = new Locale(language);
            languagesList += (languagesList.isEmpty() ? "" : ", ") + locale.getDisplayName();
        }
        return languagesList;
    }

    public String[] getTimezones() {
        return timezones;
    }

    public void setTimezones(String[] timezones) {
        this.timezones = timezones;
    }
}
