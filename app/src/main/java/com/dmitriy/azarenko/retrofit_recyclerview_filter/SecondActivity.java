package com.dmitriy.azarenko.retrofit_recyclerview_filter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

public class SecondActivity extends AppCompatActivity {
    TextView capital;
    TextView languages;
    TextView timezones;
    TextView population;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        capital = (TextView) findViewById(R.id.capital_id);
        languages = (TextView) findViewById(R.id.language_id);
        timezones = (TextView) findViewById(R.id.timezones_id);
        population = (TextView) findViewById(R.id.population_id);
        image = (ImageView) findViewById(R.id.imageViewSec_id);

        Countries countrys = (Countries) getIntent().getExtras().getSerializable("Key");

        capital.setText(countrys.getCapital());
        languages.setText(countrys.getLanguagesList());
        timezones.setText(Arrays.toString(countrys.getTimezones()).replaceAll("\\[|\\]", ""));
        population.setText(Integer.toString(countrys.getPopulation()));

    }
}
