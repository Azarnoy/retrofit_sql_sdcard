package com.dmitriy.azarenko.retrofit_recyclerview_filter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static com.dmitriy.azarenko.retrofit_recyclerview_filter.DataBaseCreator.CountriesColumns.NAME;
import static com.dmitriy.azarenko.retrofit_recyclerview_filter.DataBaseCreator.CountriesColumns.TABLE_NAME;

/**
 * Created by Дмитрий on 29.02.2016.
 */
public class DataBaseMasters {
    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;

    private static DataBaseMasters instance;


    private DataBaseMasters(Context context) {
        dbCreator = new DataBaseCreator(context);
        if (database == null || !database.isOpen()) {
            database = dbCreator.getWritableDatabase();
        }

    }

    public static DataBaseMasters getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseMasters(context);
        }
        return instance;
    }

    public void addCountry(Countries countries) {
        ContentValues cv = new ContentValues();
        cv.put(NAME, countries.name);
        database.insert(TABLE_NAME, null, cv);
        //TODO DataBaseCreator.UserColums  alt+enter and do static, and after i use just TABLE_Name and others
        Log.d("addCountries", database.insert(TABLE_NAME, null, cv) + "");


    }

    public List<Countries> getAllUsersQuery() {

        Cursor cursor = database.query(TABLE_NAME, null, null, null,
                null, null, null);

        List<Countries> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

//            что записать здесь, разобрать считывание, почему 2 одинаковых нейма в дб
            Countries countries = new Countries();
            countries.name = cursor.getString(cursor.getColumnIndex(NAME));
            Log.d("getAllUsers", "name: " + countries.name);
            list.add(countries);
            cursor.moveToNext();

        }
        cursor.close();
        return list;
    }


}
