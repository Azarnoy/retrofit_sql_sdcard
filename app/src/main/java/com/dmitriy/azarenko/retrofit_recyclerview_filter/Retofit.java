package com.dmitriy.azarenko.retrofit_recyclerview_filter;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by Дмитрий on 21.02.2016.
 */
public class Retofit  {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    interface ApiInterface {
        @GET("/v1/all")
        void getCountries(Callback<List<Countries>> callback);


    }

    static {
        init();
    }

    private static void init() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCountries(Callback<List<Countries>> callback) {
        apiInterface.getCountries(callback);
    }




}