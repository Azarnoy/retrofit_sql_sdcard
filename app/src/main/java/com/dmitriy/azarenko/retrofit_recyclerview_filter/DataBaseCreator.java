package com.dmitriy.azarenko.retrofit_recyclerview_filter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Дмитрий on 29.02.2016.
 */
public class DataBaseCreator extends SQLiteOpenHelper {

    public static final String DB_NAME = "Countries";
    public static int DB_VERSION = 1;

    public static class CountriesColumns implements BaseColumns{
        public static final String TABLE_NAME = "name";
        public static final String NAME = "country";
    }

    private static String CREATE_TABLE_COUNTRIES = "CREATE TABLE " + CountriesColumns.TABLE_NAME +
            " (" +
            CountriesColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            CountriesColumns.NAME + " TEXT" +
            ");";
    public DataBaseCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_COUNTRIES);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
