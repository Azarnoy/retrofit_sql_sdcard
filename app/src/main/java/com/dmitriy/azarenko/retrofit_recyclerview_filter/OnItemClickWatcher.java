package com.dmitriy.azarenko.retrofit_recyclerview_filter;

import android.view.View;


public abstract class OnItemClickWatcher<T> {


    public abstract void onItemClick(View v, int position, T item);
}