package com.dmitriy.azarenko.retrofit_recyclerview_filter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.stetho.Stetho;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final RecyclerView list = (RecyclerView) findViewById(R.id.listView);

        FileInputStream fis;
        try {
            fis = openFileInput("countries_file");
            ObjectInputStream ois = new ObjectInputStream(fis);
            final ArrayList<Countries> returnlist = (ArrayList<Countries>) ois.readObject();
            ois.close();

            LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
            list.setLayoutManager(layoutManager);
            list.setAdapter(new RecyclerViewAdapter(returnlist, new OnItemClickWatcher<Countries>() {
                @Override
                public void onItemClick(View v, int position, Countries item) {
                    Intent mintent = new Intent(MainActivity.this, SecondActivity.class);
                    Countries countries1 = returnlist.get(position);
                    mintent.putExtra("Key", countries1);
                    startActivity(mintent);
                }
            }));


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        Retofit.getCountries(new Callback<List<Countries>>() {
            @Override
            public void success(final List<Countries> countries, Response response) {

                FileOutputStream fos = null;
                try {
                    fos = openFileOutput("countries_file", Context.MODE_PRIVATE);
                    ObjectOutputStream oos = new ObjectOutputStream(fos);
                    oos.writeObject(countries);
                    oos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                list.setLayoutManager(layoutManager);
                list.setAdapter(new RecyclerViewAdapter(countries, new OnItemClickWatcher<Countries>() {
                    @Override
                    public void onItemClick(View v, int position, Countries item) {
                        Intent mintent = new Intent(MainActivity.this, SecondActivity.class);
                        Countries countries1 = countries.get(position);
                        mintent.putExtra("Key", countries1);
                        startActivity(mintent);
                    }
                }));

                DataBaseMasters.getInstance(MainActivity.this).addCountry(countries.get(0));




            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        });
        Stetho.initializeWithDefaults(this);

    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> implements View.OnClickListener {

        private OnItemClickWatcher<Countries> watcher;
        List<Countries> countries;


        public RecyclerViewAdapter(List<Countries> countries, OnItemClickWatcher<Countries> watcher) {
            this.countries = countries;
            this.watcher = watcher;


        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
            View v = getLayoutInflater().inflate(R.layout.list_item, parent, false);
            return new ViewHolder(v, watcher, countries);

        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int i) {
            holder.name.setText(countries.get(i).getName());
            holder.region.setText(countries.get(i).getRegion());
            holder.subregion.setText(countries.get(i).getSubregion());
            holder.image.setImageResource(countries.get(i).getImage());

        }

        @Override
        public int getItemCount() {
            return countries.size();
        }

        @Override
        public void onClick(View v) {

        }


        class ViewHolder extends RecyclerView.ViewHolder {
            private TextView name;
            private TextView region;
            private TextView subregion;
            private ImageView image;

            public ViewHolder(View itemView, final OnItemClickWatcher<Countries> watcher, final List<Countries> names) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.country_nameId);
                region = (TextView) itemView.findViewById(R.id.region_id);
                subregion = (TextView) itemView.findViewById(R.id.sub_region_id);
                image = (ImageView) itemView.findViewById(R.id.imageView_list_id);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        watcher.onItemClick(v, getAdapterPosition(), names.get(getAdapterPosition()));
                    }
                });
            }
        }

    }
}
